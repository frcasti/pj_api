require 'rails_helper'

RSpec.describe StoreProduct, type: :model do
  
  before(:each) do
  	@store = Store.create(name: 'Store name', email: 'valid.email@tets.com')
  	@product = Product.create(name: 'Product1', 
  													 sku: 'sku1', 
  													 product_type: 'Pizza', 
  													 price: 10)  
  end

	it { should belong_to(:store) }
  it { should belong_to(:product) }

  it 'not be valid' do
    store_product = StoreProduct.new
    expect(store_product).to_not be_valid
  end

  it 'be valid Assignment btw Plan and Service' do
    store_product = StoreProduct.new(store: @store, product: @product)
    expect(store_product).to be_valid
  end
end
