require 'rails_helper'

RSpec.describe Store, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }

  it { should have_many(:products) }
  it { should have_many(:orders) }

  describe 'create stores' do
	  it 'be valid' do
	  	store = Store.create(name: 'Store name', email: 'valid.email@tets.com')
	    expect(store).to be_valid
	  end

	  it 'be invalid' do
	  	store = Store.create(name: 'Store name', email: 'no.valid.email')
	    expect(store).to_not be_valid
	  end
  end

  describe 'add products to store' do
  	before(:each) do
  		@store1 = Store.create(name: 'Store name 1', email: 'valid1.email@tets.com')
  	  @product1 = Product.create(name: 'Product1', sku: 'sku1', 
  														 product_type: 'Pizza', price: 10)
  	 	@product2 = Product.create(name: 'Product2', sku: 'sku2', 
  														 product_type: 'Pizza', price: 10)
  	end
    
    it 'add valid products' do
    	@store1.add_products [@product1.id, @product2.id]
    	@store1.reload

    	expect(@store1.products.pluck(:id)).to contain_exactly(@product1.id, @product2.id)
    end

    it 'add invalid products' do
    	added_products = @store1.add_products [@product1.id, 'other']
    	@store1.reload

    	expect(@store1.products.pluck(:id)).to contain_exactly(@product1.id)
    	expect(added_products[:errors]).to_not be_empty
    end

    it 'add repeat products' do
    	@store1.add_products [@product1.id, @product2.id]

    	added_products = @store1.add_products [@product1.id]
    	@store1.reload

    	expect(@store1.products.pluck(:id)).to contain_exactly(@product1.id, @product2.id)
    	expect(added_products[:errors]).to_not be_empty
    end
  end
end
