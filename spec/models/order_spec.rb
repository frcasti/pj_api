require 'rails_helper'

RSpec.describe Order, type: :model do
  it { should have_many(:products) }
  it { should belong_to(:store) }

  before(:each) do
    @store1 = Store.create(name: 'Store name 1', email: 'valid1.email@tets.com')
    @store2 = Store.create(name: 'Store name 2', email: 'valid2.email@tets.com')
    @product1 = Product.create(name: 'Product1', sku: 'sku1', 
  														 product_type: 'Pizza', price: 10)
    @product2 = Product.create(name: 'Product2', sku: 'sku2', 
  														 product_type: 'Pizza', price: 10)
    @store1.add_products [@product1.id, @product2.id]
  end

  describe 'create orders' do
	  it 'be valid' do
	  	order = Order.create(store: @store1, price: 20, products: [@product1, @product2])
	    expect(order).to be_valid
	  end

	  it 'autocalculate price' do
	  	order = Order.create(store: @store1, products: [@product1, @product2])
	    expect(order).to be_valid
	    expect(order.price).to eq(20)
	  end
  end
end
