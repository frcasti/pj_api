require 'rails_helper'

RSpec.describe OrderProduct, type: :model do
  it { should belong_to(:order) }
  it { should belong_to(:product) }

  it 'be valid' do
  	@store1 = Store.create(name: 'Store name 1', email: 'valid1.email@tets.com')
  	@product1 = Product.create(name: 'Product1', sku: 'sku1', 
  														 product_type: 'Pizza', price: 10)
  	@store1.add_products [@product1.id]
  	@order = Order.create(store: @store1, price: 10, products: [@product1])

	  order_product = OrderProduct.create(order: @order, product: @product1)
	  expect(order_product).to be_valid
	end
end
