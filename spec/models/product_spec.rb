require 'rails_helper'

RSpec.describe Product, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:sku) }
  it { should validate_presence_of(:product_type) }
  it { should validate_presence_of(:price) }

  it { should have_many(:stores) }
  it { should have_many(:orders) }

  describe 'create products' do
	  it 'be valid' do
	  	product = Product.create(name: 'Product1', 
	  													 sku: 'sku1', 
	  													 product_type: 'Pizza', 
	  													 price: 10)
	    expect(product).to be_valid
	  end

	  it 'be valid' do
	  	product = Product.create(name: 'Product1', 
	  													 sku: 'sku1', 
	  													 product_type: 'Complement', 
	  													 price: 10)
	    expect(product).to be_valid
	  end

	  it 'be invalid with same sku' do
	  	product = Product.create(name: 'Product1', 
	  													 sku: 'sku1', 
	  													 product_type: 'Complement', 
	  													 price: 10)
	  	product2 = Product.create(name: 'Product2', 
		  													sku: 'sku1', 
		  													product_type: 'Complement', 
		  													price: 10)
	    expect(product2).to_not be_valid
	  end

	  it 'be invalid with other type' do
	  	product = Product.create(name: 'Product1', 
	  													 sku: 'sku1', 
	  													 product_type: 'other', 
	  													 price: 10)
	    expect(product).to_not be_valid
	  end

	  it 'be invalid with price <= 0' do
	  	product = Product.create(name: 'Product1', 
	  													 sku: 'sku1', 
	  													 product_type: 'Pizza', 
	  													 price: 0)
	    expect(product).to_not be_valid

	    product = Product.create(name: 'Product1', 
	    												 sku: 'sku1', 
	    												 product_type: 'Pizza', 
	    												 price: -2)
	    expect(product).to_not be_valid
	  end

	  it 'be invalid with decimal price' do
	  	product = Product.create(name: 'Product1', 
	  													 sku: 'sku1', 
	  													 product_type: 'Pizza', 
	  													 price: 12.0)
	    expect(product).to_not be_valid
	  end
  end

  describe 'add stores to store' do
  	before(:each) do
  	  @product1 = Product.create(name: 'Product1', sku: 'sku1', 
  														   product_type: 'Pizza', price: 10)
  		@store1 = Store.create(name: 'Store name 1', email: 'valid1.email@tets.com')
  		@store2 = Store.create(name: 'Store name 2', email: 'valid2.email@tets.com')
  	end
    
    it 'add valid stores' do
    	@product1.add_to_stores [@store1.id, @store2.id]
    	@product1.reload

    	expect(@product1.stores.pluck(:id)).to contain_exactly(@store1.id, @store2.id)
    end

    it 'add invalid stores' do
    	added_products = @product1.add_to_stores [@store1.id, 'other']
    	@product1.reload

    	expect(@product1.stores.pluck(:id)).to contain_exactly(@store1.id)
    	expect(added_products[:errors]).to_not be_empty
    end

    it 'add repeat stores' do
    	@product1.add_to_stores [@store1.id, @store2.id]
    	
    	added_products = @product1.add_to_stores [@store1.id]
    	@product1.reload

    	expect(@product1.stores.pluck(:id)).to contain_exactly(@store1.id, @store2.id)
    	expect(added_products[:errors]).to_not be_empty
    end
  end
end
