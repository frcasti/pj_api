require 'rails_helper'

RSpec.describe StoresController, type: :api do

  let(:valid_atts) do
    {
      name: 'Valid name',
      email: 'validemail@valid.com',
      address: 'valid address',
      phone: '9123912829'
    }
  end

  before(:each) do
    @store1 = Store.create(name: 'Store name 1', email: 'valid1.email@tets.com')
    @store2 = Store.create(name: 'Store name 2', email: 'valid2.email@tets.com')

    header 'Content-Type', 'application/json'
    header 'Accept', 'application/json'
  end

  describe "GET #index" do
    it "returns http success" do
      get '/stores.json'
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json.pluck('id')).to contain_exactly(@store1.id, @store2.id)
    end
  end

  describe "GET #show" do
    it "valid store" do
      get "/stores/#{@store1.id}.json"
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['id']).to eq(@store1.id)
      expect(json['name']).to eq('Store name 1')
    end

    it "invalid store" do
      get "/stores/invalid_store.json"
      expect(last_response.status).to eq 404
    end
  end

  describe "GET #create" do
    it "created store" do
      post '/stores.json', valid_atts.to_json
      expect(last_response.status).to eq 201

      json = JSON.parse last_response.body
      expect(json['name']).to eq('Valid name')
      expect(json['email']).to eq('validemail@valid.com')
      expect(json['address']).to eq('valid address')
      expect(json['phone']).to eq('9123912829')
    end

    it "created store by default email" do
      post '/stores.json', {name: 'Other name'}.to_json
      expect(last_response.status).to eq 201

      json = JSON.parse last_response.body
      expect(json['name']).to eq('Other name')
      expect(json['email']).to eq('francisco.abalan@pjchile.com')
    end

    it "not created store" do
      post '/stores.json', {email: 'other_email@email.com'}.to_json
      expect(last_response.status).to eq 422
    end
  end

  describe "GET #update" do
    it "update store" do
      put "/stores/#{@store1.id}.json", valid_atts.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['name']).to eq('Valid name')
      expect(json['email']).to eq('validemail@valid.com')
      expect(json['address']).to eq('valid address')
      expect(json['phone']).to eq('9123912829')
    end

    it "invalid update store" do
      put "/stores/#{@store1.id}.json", {name: nil}.to_json
      expect(last_response.status).to eq 422
    end

    it "valid with other email update store" do
      put "/stores/#{@store2.id}.json", {email: @store1.email}.to_json
      expect(last_response.status).to eq 200

      @store2.reload
      expect(@store2.email).to eq(@store1.email)
    end

    it "not update store" do
      put "/stores/ohter_id.json", {email: 'other_email@email.com'}.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe "GET #add_products" do
    before(:each) do
      @product1 = Product.create(name: 'Product1', sku: 'sku1', 
                               product_type: 'Pizza', price: 10)
      @product2 = Product.create(name: 'Product2', sku: 'sku2', 
                               product_type: 'Complement', price: 10)  
    end

    it "add products to store" do
      product_ids = {product_ids: [@product1.id, @product2.id]}

      post "/stores/#{@store1.id}/add_products.json", product_ids.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['products'].pluck('id')).to contain_exactly(@product1.id, @product2.id)
    end

    it "add repeat products to store" do
      @store1.products << @product1
      product_ids = {product_ids: [@product1.id, @product2.id]}

      post "/stores/#{@store1.id}/add_products.json", product_ids.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['products'].pluck('id')).to contain_exactly(@product1.id, @product2.id)
      expect(json['errors']).to_not be_empty
    end

    it "add invalid products to store" do
      product_ids = {product_ids: [@product1.id, 'popop']}

      post "/stores/#{@store1.id}/add_products.json", product_ids.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['products'].pluck('id')).to contain_exactly(@product1.id)
      expect(json['errors']).to_not be_empty
    end

    it "add invalid products params to store" do
      product_ids = {product_ids: 'lkjh'}

      post "/stores/#{@store1.id}/add_products.json", product_ids.to_json
      expect(last_response.status).to eq 422
    end
  end

  describe "GET #delete" do
    it 'destroys the requested store' do
      expect do
        delete "/stores/#{@store1.id}.json"
      end.to change{Store.count}.by(-1)
    end
  end
end
