require 'rails_helper'

RSpec.describe OrdersController, type: :api do

  before(:each) do
  	@product1 = Product.create(name: 'Product1', sku: 'sku1', 
  														 product_type: 'Pizza', price: 10)
  	@product2 = Product.create(name: 'Product2', sku: 'sku2', 
  														 product_type: 'Complement', price: 10)
  	@product3 = Product.create(name: 'Product3', sku: 'sku3', 
  														 product_type: 'Complement', price: 10)
    @store1 = Store.create(name: 'Store name 1', email: 'castillo.gomez82@gmail.com')
    @store2 = Store.create(name: 'Store name 2', email: 'valid2.email@tets.com')
    @store1.add_products([@product1.id, @product2.id])
    @store2.add_products([@product1.id, @product2.id])

    @order1 = Order.create(store: @store1, price: 20, products: [@product1, @product2])
    @order2 = Order.create(store: @store2, price: 20, products: [@product1, @product2])

    header 'Content-Type', 'application/json'
    header 'Accept', 'application/json'
  end

  describe "GET #index" do
    it "returns http success" do
      get "/stores/#{@store1.id}/orders.json"
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json.pluck('id')).to contain_exactly(@order1.id)
    end
  end

  describe "GET #show" do
    it "valid order" do
      get "/stores/#{@store1.id}/orders/#{@order1.id}.json"
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['id']).to eq(@order1.id)
      expect(json['price']).to eq(20)
    end

    it "invalid order" do
      get "/stores/#{@store1.id}/orders/invalid.json"
      expect(last_response.status).to eq 404

      get "/stores/#{@store1.id}/orders/#{@order2.id}.json"
      expect(last_response.status).to eq 404
    end
  end

  describe "GET #create" do
    it "created order" do
    	valid_atts = {
    		product_ids: [@product1.id, @product2.id]
    	}
      post "/stores/#{@store1.id}/orders.json", valid_atts.to_json
      expect(last_response.status).to eq 201

      json = JSON.parse last_response.body
      expect(json['store']).to eq(@store1.name)
      expect(json['price']).to eq(20)
    end

    it "created order" do
    	valid_atts = {
    		product_ids: [@product2.id]
    	}
      post "/stores/#{@store1.id}/orders.json", valid_atts.to_json
      expect(last_response.status).to eq 201

      json = JSON.parse last_response.body
      expect(json['store']).to eq(@store1.name)
      expect(json['price']).to eq(10)
    end

    it "invalid order if product is not in store" do
    	valid_atts = {
    		product_ids: [@product3.id]
    	}
      post "/stores/#{@store1.id}/orders.json", valid_atts.to_json
      expect(last_response.status).to eq 422
    end

    it "created order with multiple" do
    	valid_atts = {
    		product_ids: [@product1.id, @product2.id, @product1.id, @product2.id]
    	}
      post "/stores/#{@store1.id}/orders.json", valid_atts.to_json
      expect(last_response.status).to eq 201

      json = JSON.parse last_response.body
      expect(json['store']).to eq(@store1.name)
      expect(json['price']).to eq(40)
    end

    it "created invalid order" do
    	valid_atts = {
    		product_ids: [@product2.id, 'other']
    	}
      post "/stores/#{@store1.id}/orders.json", valid_atts.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe "GET #update" do
    it "update orders add product" do
    	valid_atts = {
    		product_ids: [@product2.id]
    	}

      put "/stores/#{@store2.id}/orders/#{@order2.id}.json", valid_atts.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['price']).to eq(10)
    end

    it "not update orders" do
    	valid_atts = {
    		product_ids: [@product2.id]
    	}

      put "/stores/#{@store2.id}/orders/ohter_id.json", valid_atts.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe "GET #delete" do
    it 'destroys the requested order' do
      expect do
        delete "/stores/#{@store1.id}/orders/#{@order1.id}.json"
      end.to change{Order.count}.by(-1)
    end
  end
end
