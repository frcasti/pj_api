require 'rails_helper'

RSpec.describe ProductsController, type: :api do

  let(:valid_atts) do
    {
      name: 'Product3',
      sku: 'sku3',
      product_type: 'Pizza',
      price: 10,
    }
  end

  before(:each) do
  	@product1 = Product.create(name: 'Product1', sku: 'sku1', 
  														 product_type: 'Pizza', price: 10)
  	@product2 = Product.create(name: 'Product2', sku: 'sku2', 
  														 product_type: 'Complement', price: 10)

    header 'Content-Type', 'application/json'
    header 'Accept', 'application/json'
  end

  describe "GET #index" do
    it "returns http success" do
      get '/products.json'
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json.pluck('id')).to contain_exactly(@product1.id, @product2.id)
    end
  end

  describe "GET #show" do
    it "valid product" do
      get "/products/#{@product1.id}.json"
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['id']).to eq(@product1.id)
      expect(json['name']).to eq('Product1')
    end

    it "invalid product" do
      get "/products/invalid_product.json"
      expect(last_response.status).to eq 404
    end
  end

  describe "GET #create" do
    it "created product" do
      post '/products.json', valid_atts.to_json
      expect(last_response.status).to eq 201

      json = JSON.parse last_response.body
      expect(json['name']).to eq('Product3')
      expect(json['sku']).to eq('sku3')
      expect(json['product_type']).to eq('Pizza')
      expect(json['price']).to eq(10)
    end

    it "invalid product with same sku" do
    	valid_atts[:sku] = 'sku1'
      post '/products.json', valid_atts.to_json
      expect(last_response.status).to eq 422
    end

    it "created invalid product" do
      post '/products.json', {name: 'Other name'}.to_json
      expect(last_response.status).to eq 422
    end
  end

  describe "GET #update" do
    it "update product" do
      put "/products/#{@product1.id}.json", valid_atts.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['name']).to eq('Product3')
      expect(json['sku']).to eq('sku3')
      expect(json['product_type']).to eq('Pizza')
      expect(json['price']).to eq(10)
    end

    it "invalid update product" do
      put "/products/#{@product1.id}.json", {name: nil}.to_json
      expect(last_response.status).to eq 422
    end

    it "invalid with other sku update product" do
      put "/products/#{@product2.id}.json", {sku: @product1.sku}.to_json
      expect(last_response.status).to eq 422
    end

    it "not update product" do
      put "/products/ohter_id.json", valid_atts.to_json
      expect(last_response.status).to eq 404
    end
  end

  describe "GET #add_to_stores" do
    before(:each) do
      @store1 = Store.create(name: 'Store name 1', email: 'valid1.email@tets.com')
    	@store2 = Store.create(name: 'Store name 2', email: 'valid2.email@tets.com')
    end

    it "add stores to product" do
      store_ids = {store_ids: [@store1.id, @store2.id]}

      post "/products/#{@product1.id}/add_to_stores.json", store_ids.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['stores'].pluck('id')).to contain_exactly(@store1.id, @store2.id)
    end

    it "add repeat stores to product" do
      @product1.stores << @store1
      store_ids = {store_ids: [@store1.id, @store2.id]}

      post "/products/#{@product1.id}/add_to_stores.json", store_ids.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['stores'].pluck('id')).to contain_exactly(@store1.id, @store2.id)
      expect(json['errors']).to_not be_empty
    end

    it "add invalid stores to product" do
      store_ids = {store_ids: [@store1.id, 'popop']}

      post "/products/#{@product1.id}/add_to_stores.json", store_ids.to_json
      expect(last_response.status).to eq 200

      json = JSON.parse last_response.body
      expect(json['stores'].pluck('id')).to contain_exactly(@store1.id)
      expect(json['errors']).to_not be_empty
    end

    it "add invalid stores params to product" do
      store_ids = {store_ids: 'lkjh'}

      post "/products/#{@product1.id}/add_to_stores.json", store_ids.to_json
      expect(last_response.status).to eq 422
    end
  end

  describe "GET #delete" do
    it 'destroys the requested product' do
      expect do
        delete "/products/#{@product1.id}.json"
      end.to change{Product.count}.by(-1)
    end
  end
end
