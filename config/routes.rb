Rails.application.routes.draw do
  resources :stores do
  	member do
  		post 'add_products', to: 'add_products'
  	end

  	resources :orders
  end

  resources :products do
  	member do
  		post 'add_to_stores', to: 'add_to_stores'
  	end
  end
end
