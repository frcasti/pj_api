class StoreMailer < ApplicationMailer
	def order_placed
		@order = Order.find(params[:order_id])
		mail(to: @order.store.email, subject: 'New order placed')
	end
end
