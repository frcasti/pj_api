class ApplicationMailer < ActionMailer::Base
  default from: 'castillo.gomez82@gmail.com'
  layout 'mailer'
end
