class StoresController < ApplicationController
	before_action :set_store, except: %i[index create]

  def index
  	@stores = Store.all

  	render json: @stores, status: :ok
  end

  def show
  	render json: @store, status: :ok
  end

  def create
  	@store = Store.new(store_params)

  	if @store.save
  		render json: @store, status: :created
  	else
  		render json: @store.errors, status: :unprocessable_entity
  	end
  end

  def update
  	if @store.update(store_params)
  		render json: @store, status: :ok
  	else
  		render json: @store.errors, status: :unprocessable_entity
  	end
  end

  def add_products
  	store_products = @store.add_products(params[:product_ids])
  	
  	if store_products[:products]
  		render json: store_products, status: :ok
  	else
  		render json: store_products, status: :unprocessable_entity
  	end
  end

  def destroy
  	if @store.destroy
  		render json: nil, status: :no_content
  	else
  		render json: @store.errors, status: :unprocessable_entity
  	end
  end

  private

  	def set_store
  		@store = Store.find(params[:id])
  	end

  	def store_params
  		params.permit(:name, :address, :email, :phone)
  	end
end
