class ProductsController < ApplicationController
	before_action :set_product, except: %i[index create]

  def index
  	@products = Product.all

  	render json: @products, status: :ok
  end

  def show
  	render json: @product, status: :ok
  end

  def create
  	@product = Product.new(poduct_params)

  	if @product.save
  		render json: @product, status: :created
  	else
  		render json: @product.errors, status: :unprocessable_entity
  	end
  end

  def update
  	if @product.update(poduct_params)
  		render json: @product, status: :ok
  	else
  		render json: @product.errors, status: :unprocessable_entity
  	end
  end

  def add_to_stores
  	product_stores = @product.add_to_stores(params[:store_ids])
  	
  	if product_stores[:stores]
  		render json: product_stores, status: :ok
  	else
  		render json: product_stores, status: :unprocessable_entity
  	end
  end

  def destroy
  	if @product.destroy
  		render json: nil, status: :no_content
  	else
  		render json: @product.errors, status: :unprocessable_entity
  	end
  end

  private

  	def set_product
  		@product = Product.find(params[:id])
  	end

  	def poduct_params
  		params.permit(:name, :sku, :product_type, :price)
  	end
end
