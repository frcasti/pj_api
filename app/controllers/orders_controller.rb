class OrdersController < ApplicationController
	before_action :set_store
	before_action :set_order, except: %i[index create]

  def index
  	@orders = @store.orders.all

  	render json: @orders, status: :ok
  end

  def show
  	render json: @order, status: :ok
  end

  def create
  	@order = @store.orders.create(order_params)

  	if @order.save
  		StoreMailer.with(order_id: @order.id).order_placed.deliver_now

  		render json: @order, status: :created
  	else
  		render json: @order.errors, status: :unprocessable_entity
  	end
  end

  def update
  	if @order.update(order_params)
  		render json: @order, status: :ok
  	else
  		render json: @order.errors, status: :unprocessable_entity
  	end
  end

  def destroy
  	if @order.destroy
  		render json: nil, status: :no_content
  	else
  		render json: @order.errors, status: :unprocessable_entity
  	end
  end

  private

  	def set_store
  		@store = Store.find(params[:store_id])
  	end

  	def set_order
  		@order = @store.orders.find(params[:id])
  	end

  	def order_params
  		params.permit(product_ids: [])
  	end
end
