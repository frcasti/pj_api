class Order < ApplicationRecord
  belongs_to :store
  has_many :order_products, dependent: :destroy
  has_many :products, through: :order_products

  validate :products_in_store

  after_save :calculate_price

  def as_json( options = {} )
  	json = super.reject { |k| k == 'store_id' }

  	options[:store] = store.name
  	options[:products] = products

  	json.merge(options)
  end

  private

  	def calculate_price
  		self.price = self.products.map(&:price).sum
  	end

  	def products_in_store
  		order_products = self.products.map(&:id)
  		store_products = self.store.nil? ? [] : self.store.products.map(&:id)

  		if order_products != store_products && !(order_products - store_products).empty?
  			errors.add(:base, 'Products are not in the store')
  		end
  	end
end
