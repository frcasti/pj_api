class Store < ApplicationRecord

	has_many :store_products, dependent: :destroy
  has_many :products, through: :store_products
  has_many :orders

	validates :name, :email, presence: true
	validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create
	
	def add_products product_ids = []
		return {error: ['Invalid params']} unless product_ids.kind_of?(Array)

		errors = []
		store_products = products.pluck(:id)
		product_ids.each do |p|
			begin
				prod = Product.find(p)

				if store_products.include?(p)
					errors << "Product: #{p} already added"
				else
					products << prod
				end
			rescue StandardError => e
				errors << "Invalid product: #{p}"
			end
		end

		{products: products, errors: errors}
	end
end
