class Product < ApplicationRecord
	TYPES = %w[Pizza Complement].freeze

	has_many :store_products, dependent: :destroy
  has_many :stores, through: :store_products
  has_many :order_products, dependent: :destroy
  has_many :orders, through: :order_products

	validates :name, :sku, :product_type, presence: true
	validates :sku, uniqueness: true
	validates :product_type, inclusion: { in: TYPES }
	validates :price,
            presence: true,
            numericality: { only_integer: true, greater_than: 0 }
            
  def add_to_stores store_ids = []
		return {error: ['Invalid params']} unless store_ids.kind_of?(Array)

		errors = []
		product_stores = stores.pluck(:id)
		store_ids.each do |s|
			begin
				store = Store.find(s)

				if product_stores.include?(s)
					errors << "Store: #{s} already added"
				else
					stores << store
				end
			rescue StandardError => e
				errors << "Invalid store: #{s}"
			end
		end

		{stores: stores, errors: errors}
	end
end
