class CreateStoreProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :store_products, id: :uuid do |t|
      t.references :store, null: false, foreign_key: true, type: :uuid
      t.references :product, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
