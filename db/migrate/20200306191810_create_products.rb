class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products, id: :uuid do |t|
      t.string :name
      t.string :sku
      t.string :product_type
      t.integer :price

      t.timestamps
    end
  end
end
