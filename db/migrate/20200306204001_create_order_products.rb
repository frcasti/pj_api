class CreateOrderProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :order_products, id: :uuid do |t|
      t.references :order, null: false, foreign_key: true, type: :uuid
      t.references :product, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
