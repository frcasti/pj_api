class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders, id: :uuid do |t|
      t.integer :price
      t.references :store, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
